# frozen_string_literal: true

require 'vbb/engine'
require 'httparty'

module Vbb
  # Implements mirr.OS hooks.
  class Hooks
    REFRESH_INTERVAL = '5m'
    API_HOST = 'fahrinfo.vbb.de'
    API_BASE = '/restproxy/2.4'
    API_KEY = '16e07734-4fe4-4209-a228-a337b0fa5445'
    PRODUCTS_BITMASK_MAP = {
      sbahn: 1,
      ubahn: 2,
      tram: 4,
      bus: 8,
      ferry: 16,
      ic: 32,
      rb: 64
    }.freeze

    # @return [String]
    def self.refresh_interval
      REFRESH_INTERVAL
    end

    # @param [Hash] configuration
    def initialize(instance_id, configuration)
      @instance_id = instance_id
      @station_id = configuration['stationId']
      @station_name = configuration['stationName']
      @chosen_transit_types = configuration['chosenTransitTypes']
    end

    def default_title
      @station_name
    end

    def configuration_valid?
      # TODO: Add credential validation if present.
      res = HTTParty.get(build_url(
                           endpoint: 'departureBoard',
                           query: {
                             maxJourneys: 1,
                             id: @station_id
                           }
                         ))
      res.success?
    rescue StandardError
      false
    end

    def list_sub_resources
      [[@station_id, @station_name]]
    end

    def fetch_data(_group, sub_resources)
      records = []
      sub_resources.each do |station_id|
        vbb_station = PublicTransport.includes(:departures).find_or_initialize_by(
          id: station_id
        ) do |station|
          station.id = station_id
          station.station_name = @station_name
        end

        vbb_station.departures.each { |d| d.mark_for_destruction if d.departure < Time.current }

        res = HTTParty.get(build_url(
                             endpoint: 'departureBoard',
                             query: build_departures_query
                           ))
        unless res.success?
          raise HTTParty::ResponseError,
                "Could not fetch departures from VBB API: #{res.code} – #{res.body}"

        end

        res.parsed_response&.dig('Departure')&.each do |departure|
          uid = departure.dig('JourneyDetailRef', 'ref')
          attributes = {
            departure: departure_time(departure),
            delay_minutes: nil, # FIXME
            line: departure.dig('Product', 'line'),
            direction: departure['direction'],
            transit_type: departure.dig('Product', 'catOut'),
            platform: departure['track']
          }
          vbb_station.update_or_insert_child(uid, attributes)
        end

        records << vbb_station
      end
      records
    end

    private

    def build_url(options = { endpoint: 'departureBoard', query: {} })
      URI::HTTPS.build(
        host: API_HOST, path: "#{API_BASE}/#{options[:endpoint]}",
        query: {
          accessId: API_KEY,
          lang: 'de', # TODO: VBB HAFAS seems to support de/en, but pulled data doesn't need translation atm
          format: 'json',
          **options[:query]
        }.to_query
      )
    end

    def departure_time(departure_obj)
      date = departure_obj['rtDate'] || departure_obj['date']
      time = departure_obj['rtTime'] || departure_obj['time']
      # VBB API probably delivers German time zone (CET/CEST)
      "#{date} #{time}".in_time_zone('Europe/Berlin') # Time.parse uses UTC
    end

    def build_departures_query
      base_query = {
        maxJourneys: 15,
        id: @station_id
        # TODO: See docs p.74 for parameters
      }
      if @chosen_transit_types.length.eql?(PRODUCTS_BITMASK_MAP.length) # all products selected
        base_query
      else
        base_query.merge(products: product_filter)
      end
    end

    def product_filter
      if @chosen_transit_types.is_a?(Array)
        @chosen_transit_types.map { |type| PRODUCTS_BITMASK_MAP[type.to_sym] }.sum
      else
        PRODUCTS_BITMASK_MAP[@chosen_transit_types.to_sym]
      end
    end
  end
end
