module Vbb
  class Engine < ::Rails::Engine
    isolate_namespace Vbb
    config.generators.api_only = true
  end
end
