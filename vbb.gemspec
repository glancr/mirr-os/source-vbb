# frozen_string_literal: true

require 'json'
$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'vbb/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name = 'vbb'
  spec.version = Vbb::VERSION
  spec.authors = ['Tobias Grasse']
  spec.email = ['tg@glancr.de']
  spec.homepage = 'https://glancr.de'
  spec.summary = 'mirr.OS data source to pull in departure information from German transport provider VBB.'
  spec.description = 'Get current departure timetables for stations of public transport provider VBB.'
  spec.license = 'MIT'
  spec.metadata = { 'json' =>
                      {
                        type: 'sources',
                        title: {
                          enGb: 'VBB Departures',
                          deDe: 'VBB Abfahrten',
                          frFr: 'VBB descentes',
                          esEs: 'VBB descensos',
                          plPl: 'VBB zjazdy',
                          koKr: 'VBB 출발'
                        },
                        description: {
                          enGb: spec.description,
                          deDe: 'Erhalte aktuelle Abfahrtszeiten für Bahnhöfe des ÖPNV-Anbieters VBB.',
                          frFr: 'Obtenez les heures de départ actuelles pour les gares du fournisseur de transport public VBB.',
                          esEs: 'Obtenga los horarios actuales de las estaciones de la compañía de transporte público VBB.',
                          plPl: 'Uzyskać aktualne czasy odjazdów dla stacji operatora transportu publicznego VBB.',
                          koKr: '대중 교통 제공자 VBB의 스테이션에 대한 현재 출발 시간을 가져옵니다.'
                        },
                        groups: ['public_transport'],
                        compatibility: '0.12.0'
                      }.to_json }

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = 'http://gems.marco-roth.ch'
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']
  spec.add_development_dependency 'rails'
  spec.add_development_dependency 'rubocop', '~> 0.81'
  spec.add_development_dependency 'rubocop-rails'
  spec.add_dependency 'httparty', '~> 0.18'
end
